package api

import java.time.LocalDate

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.{Cookie, RawHeader}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.typesafe.scalalogging.Logger
import ffs.helpers.{RequestValidator, UuidGenerator}
import ffs.models.{Document, DocumentType, DocumentVersion}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}
import ffs.services.{DocumentFilesService, DocumentService, DocumentVersionsService}
import ffs.Router
import ffs.exceptions.NotFoundException
import io.circe.Json

import scala.concurrent.Future

class DocumentsApiTest extends FlatSpec with Matchers with ScalatestRouteTest with MockFactory{

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
  import io.circe.syntax._

  val docService: DocumentService = stub[DocumentService]
  val docVersionService: DocumentVersionsService = stub[DocumentVersionsService]
  val docFilesService: DocumentFilesService = stub[DocumentFilesService]
  val helper: RequestValidator = stub[RequestValidator]
  val uuidGenerator: UuidGenerator = stub[UuidGenerator]
  val logger: Logger = Logger("FFS")
  val router = new Router(helper,docService,docVersionService,docFilesService,uuidGenerator,logger)

  val validDocVer = Map("documentType" -> "Text", "endTime" -> "2020-11-11", "title" -> "Title", "text" -> "Text", "authorKey" -> "someKey")
  val invalidDocVer = Map("documentType" -> "File", "endTime" -> "2020-11-11", "title" -> "Title", "text" -> "Text", "authorKey" -> "someKey")

  val doc = new Document(Some(1),DocumentType.Text,LocalDate.now(),"someKey")
  val docVer = new DocumentVersion(Some(1),1,1,Some("Title"), Some("Text"), None, None)

  "POST /documents" should "return new DocumentVersion" in {

    (helper.isValidDocumentCreationRequest _).when(validDocVer).returns(true)

    (docService.add _).when(validDocVer).returns(Future.successful(docVer))

    Post("/documents",validDocVer) ~> Cookie("authorKey" -> "someKey")~> router.routes ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Json] shouldBe docVer.asJson
    }
  }

  "POST /documents without cookie" should "return 401" in {
    Post("/documents",validDocVer) ~> router.routes ~> check {
      header("WWW-Authenticate") shouldEqual Some(RawHeader("WWW-Authenticate", "Custom realm=\"FFS\""))
      status shouldBe StatusCodes.Unauthorized
    }
  }

  "POST /documents with invalid data" should "return 400" in {
    (helper.isValidDocumentCreationRequest _).when(validDocVer).returns(false)

    Post("/documents", invalidDocVer) ~> Cookie("authorKey" -> "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.BadRequest
    }
  }

  "DELETE /documents/1" should "return deleted entity" in {
    (helper.isAuthor _).when(1, "someKey").returns(Future(true))

    (docService.delete _).when(1).returns(Future.successful(doc))

    Delete("/documents/1") ~> Cookie("authorKey" -> "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Json] shouldBe doc.asJson
    }
  }

  "DELETE /documents/1 without cookie" should "return 403" in {
    Delete("/documents/1") ~> router.routes ~> check {
      header("WWW-Authenticate") shouldEqual Some(RawHeader("WWW-Authenticate", "Custom realm=\"FFS\""))
      status shouldBe StatusCodes.Unauthorized
    }
  }

  "DELETE /documents/1 by unknown user" should "return 403" in {
    (helper.isAuthor _).when(1, "someKey").returns(Future(false))

    Delete("/documents/1") ~> Cookie("authorKey" -> "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.Forbidden
    }
  }

  "DELETE /documents/{nonexistentId}" should "return 404" in {
    val nonexistentId = 100500
    (helper.isAuthor _).when(nonexistentId, "someKey").throws(new NotFoundException)

    Delete(s"/documents/$nonexistentId") ~> Cookie("authorKey" -> "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.NotFound
    }
  }

}
