package api

import java.io.File
import java.nio.file.Files
import java.time.LocalDate
import java.util.UUID

import akka.http.scaladsl.model.{ContentTypes, HttpEntity, Multipart, StatusCodes}
import akka.http.scaladsl.model.headers.{Cookie, HttpCookie, HttpCookiePair, RawHeader, `Set-Cookie`}
import akka.http.scaladsl.server.directives.FileInfo
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.util.ByteString
import com.typesafe.scalalogging.Logger
import ffs.Router
import ffs.exceptions.NotFoundException
import ffs.helpers.{RequestValidator, UuidGenerator}
import ffs.models.{Document, DocumentFile, DocumentType, DocumentVersion}
import ffs.services.{DocumentFilesService, DocumentService, DocumentVersionsService}
import io.circe.Json
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.Future

class DocumentVersionsApiTest extends FlatSpec with Matchers with ScalatestRouteTest with MockFactory {
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
  import io.circe.syntax._

  val docService: DocumentService = stub[DocumentService]
  val docVersionService: DocumentVersionsService = stub[DocumentVersionsService]
  val docFilesService: DocumentFilesService = stub[DocumentFilesService]
  val helper: RequestValidator = stub[RequestValidator]
  val uuidGenerator: UuidGenerator = stub[UuidGenerator]
  val logger: Logger = Logger("FFS")
  val router = new Router(helper,docService,docVersionService,docFilesService,uuidGenerator,logger)

  val doc = new Document(Some(1),DocumentType.Text,LocalDate.now(),"someKey")
  val docVer = new DocumentVersion(Some(1),1,1,Some("Title"), Some("Text"), None, None)
  val docVerSeq = Seq(docVer, docVer.copy(versionNumber = 2))
  val mapDocVer = Map("documentId" -> "1", "versionNumber" -> "2", "title" -> "Title", "text" -> "Text")
  val docFile = new DocumentFile(Some(1), "path", "originalName")

  "GET /documentVersions?documentId=1" should "return sequence of document versions" in {
    (helper.isVersionsListAllowed _).when(1,Option(HttpCookiePair("authorKey","someKey"))).returns(Future(true))
    (docVersionService.getByDocId _).when(1).returns(Future(docVerSeq))

    Get("/documentVersion?documentId=1") ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Json] shouldBe docVerSeq.asJson
    }
  }

  "GET /documentVersions?documentId=1 without cookie" should "return 401" in {
    (helper.isVersionsListAllowed _).when(1,None).returns(Future(false))

    Get("/documentVersion?documentId=1") ~> router.routes ~> check {
      header("WWW-Authenticate") shouldEqual Some(RawHeader("WWW-Authenticate", "Custom realm=\"FFS\""))
      status shouldBe StatusCodes.Unauthorized
    }
  }

  "GET /documentVersions?documentId={nonexistentId}" should "return 404" in {
    val nonexistentId = 100500
    (helper.isVersionsListAllowed _).when(nonexistentId,Option(HttpCookiePair("authorKey","someKey"))).throws(new NotFoundException)

    Get(s"/documentVersion?documentId=$nonexistentId") ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.NotFound
    }
  }

  "POST /documentVersion" should "return document version" in {
    val newDocVer = docVer.copy(versionNumber = 2)
    val addedNewDocVer = newDocVer.copy(id = Some(2))
    (helper.isValidDocumentVersionCreationRequest _).when(newDocVer).returns(Future(true))
    (docVersionService.add _).when(newDocVer).returns(Future.successful(addedNewDocVer))

    Post("/documentVersion", newDocVer) ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Json] shouldBe addedNewDocVer.asJson
    }
  }

  "POST /documentVersion with invalid data" should "return 400" in {
    val newDocVer = docVer.copy(versionNumber = 100000)
    (helper.isValidDocumentVersionCreationRequest _).when(newDocVer).returns(Future(false))

    Post("/documentVersion", newDocVer) ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.BadRequest
    }
  }

  "GET /documentVersion/1" should "return document version" in {
    (helper.isVersionAllowed _).when(1,Option(HttpCookiePair("authorKey", "someKey"))).returns(Future(true))

    (docVersionService.get _).when(1).returns(Future(docVer))

    Get("/documentVersion/1") ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Json] shouldBe docVer.asJson
    }
  }

  "GET /documentVersion/1 without cookie" should "return 401" in {
    (helper.isVersionAllowed _).when(1, None).returns(Future(false))

    Get("/documentVersion/1") ~> router.routes ~> check {
      header("WWW-Authenticate") shouldEqual Some(RawHeader("WWW-Authenticate", "Custom realm=\"FFS\""))
      status shouldBe StatusCodes.Unauthorized
    }
  }

  "GET /documentVersion/1 without cookie" should "return document version" in {
    (helper.isVersionAllowed _).when(1, None).returns(Future(true))

    (docVersionService.get _).when(1).returns(Future(docVer))

    Get("/documentVersion/1") ~> router.routes ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Json] shouldBe docVer.asJson
    }
  }

  "DELETE /documentVersion/1" should "return document version" in {
    (helper.isVersionAuthor _).when(1,"someKey").returns(Future(true))

    (docVersionService.delete _).when(1).returns(Future(docVer))

    Delete("/documentVersion/1") ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Json] shouldBe docVer.asJson
    }
  }

  "DELETE /documentVersion/1 without cookie" should "return 401" in {
    Delete("/documentVersion/1") ~> router.routes ~> check {
      header("WWW-Authenticate") shouldEqual Some(RawHeader("WWW-Authenticate", "Custom realm=\"FFS\""))
      status shouldBe StatusCodes.Unauthorized
    }
  }

  "DELETE /documentVersion/1 sent by unknown user" should "return 403" in {
    (helper.isVersionAuthor _).when(1,"someKey").returns(Future(false))

    Delete("/documentVersion/1") ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.Forbidden
    }
  }

  "GET /documentVersion/{id}/openAccess" should "return link" in {
    val id = 1
    (helper.isVersionAuthor _).when(id,"someKey").returns(Future(true))

    (docVersionService.openAccess _).when(id).returns(Future("string"))

    Get(s"/documentVersion/$id/openAccess") ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.OK
      responseAs[String] shouldBe "string"
    }
  }

  "GET /documentVersion/{id}/openAccess sent by unknown user" should "return 403" in {
    val id = 1
    (helper.isVersionAuthor _).when(id,"someKey").returns(Future(false))

    Get(s"/documentVersion/$id/openAccess") ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.Forbidden
    }
  }

  "GET /documentVersion/{id}/closeAccess" should "return link" in {
    val id = 1
    (helper.isVersionAuthor _).when(id,"someKey").returns(Future(true))

    (docVersionService.closeAccess _).when(id).returns(Future(1))

    Get(s"/documentVersion/$id/closeAccess") ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Int] shouldBe 1
    }
  }

  "GET /documentVersion/{id}/closeAccess sent by unknown user" should "return 403" in {
    val id = 1
    (helper.isVersionAuthor _).when(id,"someKey").returns(Future(false))

    Get(s"/documentVersion/$id/openAccess") ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.Forbidden
    }
  }

  "POST /documentVersion/{id}/attachFile" should "return document version with file" in {
    val id = 1

    (helper.isVersionAuthor _).when(id, "someKey").returns(Future(true))
    (helper.canAttach _).when(id).returns(Future(true))
    (helper.isValidFile _).when(*).returns(true)
    (docVersionService.attachFile _).when(*,*,*).returns(Future((docVer.copy(text = None, fileId = Some(1)),docFile)))

    val multipartForm =
      Multipart.FormData(Multipart.FormData.BodyPart.Strict(
        "file",
        HttpEntity(ContentTypes.`text/plain(UTF-8)`, "2,3,5\n7,11,13,17,23\n29,31,37\n"),
        Map("filename" -> "originalName")))

    Post(s"/documentVersion/$id/attachFile", multipartForm) ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Json] shouldBe (docVer.copy(text = None, fileId = Some(1)),docFile).asJson
    }
  }

  "POST /documentVersion/{id}/attachFile sent by unknown user" should "return 403" in {
    val id = 1

    (helper.isVersionAuthor _).when(id, "someKey").returns(Future(false))

    val multipartForm =
      Multipart.FormData(Multipart.FormData.BodyPart.Strict(
        "file",
        HttpEntity(ContentTypes.`text/plain(UTF-8)`, "2,3,5\n7,11,13,17,23\n29,31,37\n"),
        Map("filename" -> "originalName")))

    Post(s"/documentVersion/$id/attachFile", multipartForm) ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.Forbidden
    }
  }


  "POST /documentVersion/{id}/attachFile for filled version" should "return 400" in {
    val id = 1

    (helper.isVersionAuthor _).when(id, "someKey").returns(Future(true))
    (helper.canAttach _).when(id).returns(Future(false))

    val multipartForm =
    Multipart.FormData(Multipart.FormData.BodyPart.Strict(
    "file",
        HttpEntity(ContentTypes.`text/plain(UTF-8)`, "2,3,5\n7,11,13,17,23\n29,31,37\n"),
        Map("filename" -> "originalName")))

    Post(s"/documentVersion/$id/attachFile", multipartForm) ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.BadRequest
    }
  }

  "POST /documentVersion/{id}/attachFile for invalid file" should "return 400" in {
    val id = 1

    (helper.isVersionAuthor _).when(id, "someKey").returns(Future(true))
    (helper.canAttach _).when(id).returns(Future(true))
    (helper.isValidFile _).when(*).returns(false)

    val multipartForm =
      Multipart.FormData(Multipart.FormData.BodyPart.Strict(
        "file",
        HttpEntity(ContentTypes.`application/octet-stream`, ByteString("some bytes")),
        Map("filename" -> "originalName")))

    Post(s"/documentVersion/$id/attachFile", multipartForm) ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.BadRequest
    }
  }

  "POST /documentVersion/{id}/attachFile with nonexistentId" should "return 404" in {
    val id = 1

    (helper.isVersionAuthor _).when(id, "someKey").throws(new NotFoundException)

    val multipartForm =
      Multipart.FormData(Multipart.FormData.BodyPart.Strict(
        "file",
        HttpEntity(ContentTypes.`text/plain(UTF-8)`, "2,3,5\n7,11,13,17,23\n29,31,37\n"),
        Map("filename" -> "originalName")))

    Post(s"/documentVersion/$id/attachFile", multipartForm) ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.NotFound
    }
  }

//  "GET /file/{id}" should "return file" in {
//    (helper.isFileAllowed _).when(1,Option(HttpCookiePair("authorKey", "someKey"))).returns(Future(true))
//    (docFilesService.get _).when(1).returns(Future(docFile))
//
//    Get("/file/1") ~> router.routes ~> check {
//      responseAs[String] shouldEqual "example file contents"
//    }
//  }

  "GET /auth" should "return cookie" in {
    val uuid = UUID.randomUUID()
    (uuidGenerator.get _).when().returns(uuid)

    Get("/auth") ~> router.routes ~> check {
      responseAs[String] shouldEqual "Authorized"
      header[`Set-Cookie`] shouldEqual Some(`Set-Cookie`(HttpCookie("authorKey", value = uuid.toString)))
    }
  }

  "GET /all" should "return data" in {
    val resp = (Seq(doc),Seq(docVerSeq))
    (docService.index _).when("someKey").returns(Future(resp))

    Get("/all") ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      responseAs[Json] shouldBe resp.asJson
    }
  }

  "GET /all without cookie" should "return 401" in {
    Get("/all") ~> router.routes ~> check {
      header("WWW-Authenticate") shouldEqual Some(RawHeader("WWW-Authenticate", "Custom realm=\"FFS\""))
      status shouldBe StatusCodes.Unauthorized
    }
  }

  "GET /all" should "return 500 for unexpected exception" in {
    (docService.index _).when("someKey").throws(new NullPointerException)

    Get("/all") ~> Cookie("authorKey", "someKey") ~> router.routes ~> check {
      status shouldBe StatusCodes.InternalServerError
    }
  }
}
