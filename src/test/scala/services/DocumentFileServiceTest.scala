package services

import akka.http.scaladsl.testkit.ScalatestRouteTest
import ffs.database.operations.DocumentFilesOperations
import ffs.exceptions.NotFoundException
import ffs.models.DocumentFile
import ffs.services.DocumentFilesService
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.Future

class DocumentFileServiceTest extends FlatSpec with Matchers with ScalatestRouteTest with MockFactory{
  val documentFilesOperations: DocumentFilesOperations = stub[DocumentFilesOperations]
  val documentFilesService = new DocumentFilesService(documentFilesOperations)

  val docFile = new DocumentFile(Some(0),"path", "filename")

  "add" should "return document file" in {
    val returned = docFile.copy(id = Some(1))
    (documentFilesOperations.add _).when(docFile).returns(Future(returned))

    documentFilesService.add(docFile).map {
      _ shouldBe returned
    }
  }

  "get" should "return document file" in {
    val returned = docFile.copy(id = Some(1))
    (documentFilesOperations.get _).when(1).returns(Future(returned))

    documentFilesService.get(1).map {
      _ shouldBe returned
    }
  }

  "get" should "throw NotFoundException" in {
    (documentFilesOperations.get _).when(1).throws(new NotFoundException)

    assertThrows[NotFoundException] {
      documentFilesService.get(1)
    }
  }
}
