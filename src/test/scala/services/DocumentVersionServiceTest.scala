package services

import java.io.File
import java.nio.file.Files

import akka.http.scaladsl.model.{ContentType, ContentTypes}
import akka.http.scaladsl.server.directives.FileInfo
import akka.http.scaladsl.testkit.ScalatestRouteTest
import ffs.database.operations.DocumentVersionsOperations
import ffs.exceptions.NotFoundException
import ffs.helpers.LinkGenerator
import ffs.models.{DocumentFile, DocumentVersion}
import ffs.services.{DocumentFilesService, DocumentVersionsService}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.Future

class DocumentVersionServiceTest extends FlatSpec with Matchers with ScalatestRouteTest with MockFactory {
  val documentVersionsOperations: DocumentVersionsOperations = stub[DocumentVersionsOperations]
  val documentFilesService: DocumentFilesService = stub[DocumentFilesService]
  val linkGenerator: LinkGenerator = stub[LinkGenerator]

  val docVer = new DocumentVersion(Some(0),1,1,Some("Title"), Some("Text"), None, Some(false))
  val docFile = new DocumentFile(Some(0),"path", "filename")

  val documentVersionsService = new DocumentVersionsService(documentVersionsOperations,documentFilesService,linkGenerator)

  "add" should "return document version" in {
    (documentVersionsOperations.add _).when(docVer).returns(Future(docVer.copy(id = Some(1))))

    documentVersionsService.add(docVer).map {
      _ shouldBe docVer.copy(id = Some(1))
    }
  }

  "addNewVersion" should "return document version" in {
    val returned = docVer.copy(id = Some(1), versionNumber = 2)
    (documentVersionsOperations.add _).when(docVer.copy(versionNumber = 2)).returns(Future(returned))

    documentVersionsService.addNewVersion(docVer).map { doc =>
      doc shouldBe returned
    }
  }

  "delete" should "return deleted document version" in {
    (documentVersionsOperations.delete _).when(1).returns(Future(docVer.copy(id = Some(1))))

    documentVersionsService.delete(1).map {
      _ shouldBe docVer.copy(id = Some(1))
    }
  }

  "get" should "return document version" in {
    (documentVersionsOperations.get _).when(1).returns(Future(docVer.copy(id = Some(1))))

    documentVersionsService.get(1).map {
      _ shouldBe docVer.copy(id = Some(1))
    }
  }

  "get" should "throw NotFoundException" in {
    (documentVersionsOperations.get _).when(1).throws(new NotFoundException)

    assertThrows[NotFoundException] {
      documentVersionsService.get(1)
    }
  }

  "getByDocId" should "return document version" in {
    (documentVersionsOperations.getAllByDocumentId _).when(1).returns(Future(Seq(docVer.copy(id = Some(1)))))

    documentVersionsService.getByDocId(1).map {
      _ shouldBe docVer.copy(id = Some(1))
    }
  }

  "getByDocId" should "throw NotFoundException" in {
    (documentVersionsOperations.getAllByDocumentId _).when(1).throws(new NotFoundException)

    assertThrows[NotFoundException] {
      documentVersionsService.getByDocId(1)
    }
  }

  "openAccess" should "return String" in {
    val result = "correct string"
    val id = 1
    val rawCount = 1
    (linkGenerator.generateLinkOnDocVersion _).when(id).returns(result)
    (documentVersionsOperations.openAccess _).when(id).returns(Future(rawCount))

    documentVersionsService.openAccess(id).map {
      _ shouldBe result
    }
  }

  "closeAccess" should "return rawCount" in {
    val id = 1
    val rawCount = 1

    (documentVersionsOperations.closeAccess _).when(id).returns(Future(rawCount))

    documentVersionsService.closeAccess(id).map {
      _ shouldBe rawCount
    }
  }

  "attachFile" should "return correct data" in {
    val addedFile = docFile.copy(id = Some(1))
    val result = (docVer,addedFile)
    (documentFilesService.add _).when(*).returns(Future(addedFile))
    (documentVersionsOperations.attachFile _).when(docVer.id.get,addedFile.id.get).returns(Future(result))

    val tmpFile = Files.createTempFile("ffs_",".tmp")

    documentVersionsService.attachFile(
      docVer.id.get,
      tmpFile.toFile,
      FileInfo("file","filename.txt", ContentTypes.`text/plain(UTF-8)`)
    ).map {
      _ shouldBe result
    }
  }
}
