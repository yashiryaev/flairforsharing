package services

import java.time.LocalDate

import akka.http.scaladsl.testkit.ScalatestRouteTest
import ffs.database.operations.{DocumentVersionsOperations, DocumentsOperations}
import ffs.exceptions.NotFoundException
import ffs.models.{Document, DocumentType, DocumentVersion}
import ffs.services.{DocumentService, DocumentVersionsService}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.Future
import scala.util.Failure

class DocumentServiceTest extends FlatSpec with Matchers with ScalatestRouteTest with MockFactory{
  val docVerService: DocumentVersionsService = stub[DocumentVersionsService]
  val docOperations: DocumentsOperations = stub[DocumentsOperations]
  val docVerOperations: DocumentVersionsOperations = stub[DocumentVersionsOperations]
  val docService = new DocumentService(docVerService,docOperations,docVerOperations)

  val doc = new Document(None,DocumentType.Text,LocalDate.now(),"someKey")
  val docMap = Map("documentType" -> "Text", "title"->"Title", "text"->"Text", "endTime" -> LocalDate.now().toString, "authorKey"->"someKey")
  val docVer = new DocumentVersion(Some(0),1,1,Some("Title"), Some("Text"), None, Some(false))

  "add" should "return document versions" in {
    (docOperations.add _).when(doc).returns(Future(doc.copy(id = Some(1))))
    (docVerService.add _).when(docVer).returns(Future(docVer.copy(id = Some(1))))

    docService.add(docMap).map(doc => doc shouldBe docVer.copy(id = Some(1)))
  }

  "index" should "return correct data" in {
    val authorKey = "someKey"
    val returnedValue = (Seq(doc.copy(id = Some(1))),Seq(docVer.copy(id = Some(1))))
    (docOperations.index _).when(authorKey).returns(Future(returnedValue._1))
    (docVerOperations.getAllByDocumentId _).when(1).returns(Future(returnedValue._2))

    docService.index(authorKey).map(_ shouldBe returnedValue)
  }

  "index for nonexistentId" should "throw NotFoundException" in {
    val authorKey = "someKey"
    val id = 1
    val returnedValue = Seq(doc.copy(id = Some(id)))
    (docOperations.index _).when(authorKey).returns(Future(returnedValue))
    (docVerOperations.getAllByDocumentId _).when(id).throws(new NotFoundException)

    docService.index(authorKey).map(_ shouldBe (returnedValue, Failure(new NotFoundException)))
  }

  "delete" should "return deleted doc" in {
    val id = 1

    (docOperations.delete _).when(id).returns(Future(doc.copy(id = Some(1))))

    docService.delete(id).map(_ shouldBe doc.copy(id = Some(1)))
  }

  "delete" should "throw NotFoundException" in {
    val id = 1

    (docOperations.delete _).when(id).throws(new NotFoundException)

    assertThrows[NotFoundException] {
      docService.delete(id)
    }
  }
}
