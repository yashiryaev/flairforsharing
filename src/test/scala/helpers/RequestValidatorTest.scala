package helpers

import java.io.{File, RandomAccessFile}
import java.nio.ByteBuffer
import java.nio.channels.SeekableByteChannel
import java.nio.file.{Files, Paths, StandardOpenOption}
import java.time.LocalDate

import akka.http.scaladsl.model.ContentTypes
import akka.http.scaladsl.model.headers.HttpCookiePair
import akka.http.scaladsl.server.directives.FileInfo
import akka.http.scaladsl.testkit.ScalatestRouteTest
import ffs.database.operations.{DocumentVersionsOperations, DocumentsOperations}
import ffs.exceptions.NotFoundException
import ffs.helpers.RequestValidator
import ffs.models.{Document, DocumentType, DocumentVersion}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{AsyncFlatSpec, AsyncFlatSpecLike, FlatSpec, Matchers}

import scala.concurrent.Future

class RequestValidatorTest extends FlatSpec with Matchers with ScalatestRouteTest with MockFactory {
  val documentsOperations: DocumentsOperations = stub[DocumentsOperations]
  val documentVersionsOperations: DocumentVersionsOperations = stub[DocumentVersionsOperations]
  val requestValidator = new RequestValidator(documentsOperations, documentVersionsOperations)

  val validDocMap = Map("documentType" -> "Text", "title"->"Title", "text"->"Text", "endTime" -> LocalDate.now().toString, "authorKey"->"someKey")
  val wrongTypeDocMap = Map("documentType" -> "File", "title"->"Title", "text"->"Text", "endTime" -> LocalDate.now().toString, "authorKey"->"someKey")
  val nonexistentTypeDocMap = Map("documentType" -> "Kek", "title"->"Title", "text"->"Text", "endTime" -> LocalDate.now().toString, "authorKey"->"someKey")
  val withoutRequiredFieldDocMap = Map( "title"->"Title", "text"->"Text", "endTime" -> LocalDate.now().toString, "authorKey"->"someKey")

  val docVer = new DocumentVersion(Some(0),1,1,Some("Title"), Some("Text"), None, Some(false))
  val doc = new Document(None,DocumentType.Text,LocalDate.now(),"someKey")

  "isValidDocumentCreationRequest" should "return true" in {
    requestValidator.isValidDocumentCreationRequest(validDocMap) shouldBe true
  }

  "isValidDocumentCreationRequest for map with wrong document type" should "return false" in {
    requestValidator.isValidDocumentCreationRequest(wrongTypeDocMap) shouldBe false
  }

  "isValidDocumentCreationRequest for map without required field" should "return false" in {
    requestValidator.isValidDocumentCreationRequest(withoutRequiredFieldDocMap) shouldBe false
  }

  "isValidDocumentCreationRequest for map with nonexistent document type" should "return false" in {
    requestValidator.isValidDocumentCreationRequest(nonexistentTypeDocMap) shouldBe false
  }

  "isValidFile" should "return true" in {
    val tmpFile = Files.createTempFile("ffs_",".tmp").toFile
    val fileInfo = FileInfo("file","filename.txt", ContentTypes.`text/plain(UTF-8)`)

    requestValidator.isValidFile((fileInfo,tmpFile)) shouldBe true
  }

  "isValidFile for executable file" should "return false" in {
    val tmpFile = Files.createTempFile("ffs_",".tmp").toFile
    val fileInfo = FileInfo("file","filename.exe", ContentTypes.`text/plain(UTF-8)`)

    requestValidator.isValidFile((fileInfo,tmpFile)) shouldBe false
  }

  "isValidFile for executable mac file" should "return false" in {
    val tmpFile = Files.createTempFile("ffs_",".tmp").toFile
    val fileInfo = FileInfo("file","filename.app", ContentTypes.`text/plain(UTF-8)`)

    requestValidator.isValidFile((fileInfo,tmpFile)) shouldBe false
  }

  "isValidFile for huge file" should "return false" in {
    val fileInfo = FileInfo("file","filename.txt", ContentTypes.`text/plain(UTF-8)`)

    val buf = ByteBuffer.allocate(4).putInt(2)
    buf.rewind
    val hugeFile = Paths.get("hugeFile.txt")

    val channel= Files.newByteChannel(hugeFile,StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW, StandardOpenOption.SPARSE)

    channel.position(1024*1024*6)
    channel.write(buf)
    channel.close()

    requestValidator.isValidFile((fileInfo,hugeFile.toFile)) shouldBe false
  }

  "isValidDocumentVersionCreationRequest" should "return true" in {
    (documentsOperations.get _).when(docVer.documentId).returns(Future(doc.copy(id = Some(1))))
    (documentVersionsOperations.getAllByDocumentId _).when(docVer.documentId).returns(Future(Seq(docVer.copy(id = Some(1)))))

    requestValidator.isValidDocumentVersionCreationRequest(docVer).map {
      _ shouldBe true
    }
  }

  "isValidDocumentVersionCreationRequest" should "return false" in {
    (documentsOperations.get _).when(docVer.documentId).throws(new NotFoundException)
    (documentVersionsOperations.getAllByDocumentId _).when(docVer.documentId).returns(Future(Seq(docVer.copy(id = Some(1)))))

    assertThrows[NotFoundException] {
      requestValidator.isValidDocumentVersionCreationRequest(docVer)
    }
  }

  "isValidDocumentVersionCreationRequest for doc version with incorrect version number" should "return false" in {
    (documentsOperations.get _).when(docVer.documentId).returns(Future(doc.copy(id = Some(1))))
    (documentVersionsOperations.getAllByDocumentId _).when(docVer.documentId).returns(Future(Seq(docVer.copy(id = Some(1)))))


    requestValidator.isValidDocumentVersionCreationRequest(docVer.copy(versionNumber = 100)).map {
      _ shouldBe false
    }

  }

  "isValidDocumentVersionCreationRequest for doc version with incorrect type" should "return false" in {
    (documentsOperations.get _).when(docVer.documentId).returns(Future(doc.copy(id = Some(1))))
    (documentVersionsOperations.getAllByDocumentId _).when(docVer.documentId).returns(Future(Seq(docVer.copy(id = Some(1)))))


    requestValidator.isValidDocumentVersionCreationRequest(docVer.copy(fileId = Some(1))).map {
      _ shouldBe false
    }

  }

  "isVersionListAllowed" should "return true" in {
    val docId = 1
    val cookie = Option(HttpCookiePair("authorKey", "someKey"))

    (documentsOperations.get _).when(docId).returns(Future(doc))
    (documentVersionsOperations.getAllByDocumentId _).when(docId)
      .returns(
        Future(
          Seq(
            docVer.copy(isAllowedAccess = Some(true)))))

    requestValidator.isVersionsListAllowed(1,cookie).map {
      _ shouldBe true
    }
  }

  "isVersionListAllowed without cookie" should "return true" in {
    val docId = 1

    (documentsOperations.get _).when(docId).returns(Future(doc))
    (documentVersionsOperations.getAllByDocumentId _).when(docId)
      .returns(
        Future(
          Seq(
            docVer.copy(isAllowedAccess = Some(true)))))

    requestValidator.isVersionsListAllowed(1,None).map {
      _ shouldBe true
    }
  }

  "isVersionListAllowed for unknown user" should "return true" in {
    val docId = 1
    val cookie = Option(HttpCookiePair("authorKey", "someOtherKey"))

    (documentsOperations.get _).when(docId).returns(Future(doc))
    (documentVersionsOperations.getAllByDocumentId _).when(docId)
      .returns(
        Future(
          Seq(
            docVer.copy(isAllowedAccess = Some(true)))))

    requestValidator.isVersionsListAllowed(1,cookie).map{ x =>
      x shouldBe true
    }
  }

  "isVersionListAllowed for not accessible list" should "return true for author" in {
    val docId = 1
    val cookie = Option(HttpCookiePair("authorKey", "someKey"))

    (documentsOperations.get _).when(docId).returns(Future(doc))
    (documentVersionsOperations.getAllByDocumentId _).when(docId)
      .returns(
        Future(
          Seq(
            docVer.copy(isAllowedAccess = Some(false)))))

    requestValidator.isVersionsListAllowed(1,cookie).map {
      _ shouldBe true
    }
  }

  "isVersionListAllowed for not accessible list" should "return false for unknown user" in {
    val docId = 1
    val cookie = Option(HttpCookiePair("authorKey", "someKey"))

    (documentsOperations.get _).when(docId).returns(Future(doc))
    (documentVersionsOperations.getAllByDocumentId _).when(docId)
      .returns(
        Future(
          Seq(
            docVer.copy(isAllowedAccess = Some(false)))))

    requestValidator.isVersionsListAllowed(1,cookie).map {
      _ shouldBe false
    }
  }

  "isFileAllowed" should "return true" in {
    val cookie = Option(HttpCookiePair("authorKey", "someKey"))

    (documentVersionsOperations.get _).when(1).returns(Future(docVer.copy(id = Some(1))))
    (documentVersionsOperations.getByFileId _).when(1).returns(Future(docVer.copy(id = Some(1))))

    requestValidator.isFileAllowed(1,cookie).map {
      _ shouldBe true
    }

  }

}
