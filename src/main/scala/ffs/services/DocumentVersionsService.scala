package ffs.services

import java.io.File
import java.nio.file.{Files, StandardCopyOption}
import java.util.UUID

import ffs.database.operations.DocumentVersionsOperations
import ffs.helpers.LinkGenerator
import ffs.models.{DocumentFile, DocumentVersion}
import akka.http.scaladsl.server.directives.FileInfo

import scala.concurrent.{ExecutionContext, Future}

class DocumentVersionsService(
                               documentVersionsOperations: DocumentVersionsOperations,
                               documentFilesService: DocumentFilesService,
                               linkGenerator: LinkGenerator
                             )(implicit ec: ExecutionContext) {

  def add(documentVersion: DocumentVersion): Future[DocumentVersion] = {
    documentVersionsOperations.add(documentVersion)
  }

  def addNewVersion(documentVersion: DocumentVersion): Future[DocumentVersion] = {
    documentVersionsOperations.add(documentVersion.copy(versionNumber = documentVersion.versionNumber + 1))
  }

  def delete(id: Int): Future[DocumentVersion] = {
    documentVersionsOperations.delete(id)
  }

  def get(id: Int): Future[DocumentVersion] = {
    documentVersionsOperations.get(id)
  }

  def getByDocId(docId: Int): Future[Seq[DocumentVersion]] = {
    documentVersionsOperations.getAllByDocumentId(docId)
  }

  def openAccess(id: Int): Future[String] = {
    documentVersionsOperations.openAccess(id).map(_ => linkGenerator.generateLinkOnDocVersion(id))
  }

  def closeAccess(id: Int): Future[Int] = {
    documentVersionsOperations.closeAccess(id)
  }

  def attachFile(versionId: Int, file: File, meta: FileInfo): Future[(DocumentVersion, DocumentFile)] = {
    val ext = meta.fileName.split("\\.").last
    val newPath = new File(new File(".").getCanonicalPath + "/storage/" + UUID.randomUUID() + "." + ext).toPath

    Files.move(
      file.toPath,
      newPath,
      StandardCopyOption.REPLACE_EXISTING
    )

    file.delete

    val newFile = documentFilesService.add(new DocumentFile(Some(0),newPath.toString, meta.fileName))

    newFile flatMap { file =>
      documentVersionsOperations.attachFile(versionId, file.id.get)
    }
  }
}
