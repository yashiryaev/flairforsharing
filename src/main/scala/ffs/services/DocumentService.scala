package ffs.services

import java.time.{LocalDate, LocalTime}
import java.time.format.DateTimeFormatter

import ffs.database.operations.{DocumentVersionsOperations, DocumentsOperations}
import ffs.models.{Document, DocumentType, DocumentVersion}

import scala.concurrent.{ExecutionContext, Future}

class DocumentService(
                       documentVersionsService: DocumentVersionsService,
                       documentsOperations: DocumentsOperations,
                       documentVersionsOperations: DocumentVersionsOperations
                     )(implicit ec: ExecutionContext) {

  def add(documentFields: Map[String, String]): Future[DocumentVersion] = {
    val doc = documentFields("documentType") match {
      case "Text" => new Document(id = None, documentType = DocumentType.Text, endTime = LocalDate.parse(documentFields("endTime"), DateTimeFormatter.ISO_LOCAL_DATE), authorKey = documentFields("authorKey"))
      case "File" => new Document(id = None, documentType = DocumentType.File, endTime = LocalDate.parse(documentFields("endTime"), DateTimeFormatter.ISO_LOCAL_DATE), authorKey = documentFields("authorKey"))
    }

    val newDoc = documentsOperations.add(doc)

    newDoc flatMap { doc =>
      documentVersionsService.add(
        new DocumentVersion(
          Some(0),
          doc.id.get,
          1,
          documentFields.get("title"),
          documentFields.get("text"),
          None,
          Some(false)
        )
      )
    }
  }

  def index(authorKey: String): Future[(Seq[Document], Seq[Seq[DocumentVersion]])] = { //
    val docs: Future[Seq[Document]] = documentsOperations.index(authorKey)
    (docs,
      docs.map(_.map(_.id))
        .map(_.map(id => documentVersionsOperations.getAllByDocumentId(id.get)))
        .flatMap(Future.sequence(_))) match {
      case (f1, f2) =>
        for {
          s1 <- f1
          s2 <- f2
        } yield (s1, s2)
    }
  }

  def delete(id: Int): Future[Document] = {
    documentsOperations.delete(id)
  }
}
