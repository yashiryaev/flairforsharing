package ffs.services

import ffs.database.operations.DocumentFilesOperations
import ffs.models.DocumentFile

import scala.concurrent.Future

class DocumentFilesService(documentFilesOperations: DocumentFilesOperations) {
  def add(file: DocumentFile): Future[DocumentFile] = {
    documentFilesOperations.add(file)
  }

  def get(id: Int):Future[DocumentFile] = {
    documentFilesOperations.get(id)
  }
}
