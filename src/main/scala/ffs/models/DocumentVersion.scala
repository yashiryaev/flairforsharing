package ffs.models

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._
import slick.lifted.Tag
import slick.jdbc.PostgresProfile.api._

import scala.language.higherKinds

case class DocumentVersion(
                            id: Option[Int],
                            documentId: Int,
                            versionNumber: Int,
                            title: Option[String],
                            text: Option[String],
                            fileId: Option[Int],
                            isAllowedAccess: Option[Boolean]
                          )

object DocumentVersion {
  implicit val encoder: Encoder[DocumentVersion] = deriveEncoder
  implicit val decoder: Decoder[DocumentVersion] = deriveDecoder

  val tupled = (this.apply _).tupled
}


class DocumentVersions(tag: Tag) extends Table[DocumentVersion](tag, "documentVersions") {
  def id: Rep[Option[Int]] = column[Option[Int]]("id", O.PrimaryKey, O.AutoInc)

  def documentId: Rep[Int] = column[Int]("documentId")

  def versionNumber: Rep[Int] = column[Int]("versionNumber")

  def title: Rep[Option[String]] = column[Option[String]]("title")

  def text: Rep[Option[String]] = column[Option[String]]("text")

  def fileId: Rep[Option[Int]] = column[Option[Int]]("fileId")

  def isAllowedAccess: Rep[Option[Boolean]] = column[Option[Boolean]]("isAllowedAccess", O.Default(Some(false)))

  def document = foreignKey("document_fk", documentId, DocumentsRepository.all)(_.id.get, onUpdate = ForeignKeyAction.Cascade, onDelete = ForeignKeyAction.Cascade)

  def file = foreignKey("file_fk", fileId, DocumentFilesRepository.all)(_.id.get, onUpdate = ForeignKeyAction.Cascade, onDelete = ForeignKeyAction.Restrict)

  override def * =
    (
      id,
      documentId,
      versionNumber,
      title,
      text,
      fileId,
      isAllowedAccess
    ) <> (DocumentVersion.tupled, DocumentVersion.unapply)
}

object DocumentVersionsRepository {
  val all = TableQuery[DocumentVersions]

  implicit class DocumentVersionExtensions[C[_]](q: Query[DocumentVersions, DocumentVersion, C]) {
    def withDocument = q.join(DocumentsRepository.all).on(_.documentId === _.id)
    def withFile = q.join(DocumentFilesRepository.all).on(_.fileId === _.id)
  }
}