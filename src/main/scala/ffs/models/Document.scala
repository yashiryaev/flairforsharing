package ffs.models

import java.time.LocalDate

import enumeratum.{CirceEnum, Enum, EnumEntry, SlickEnumSupport}
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._
import slick.lifted.Tag
import slick.jdbc.PostgresProfile.api._

import scala.collection.immutable

case class Document(
                     id: Option[Int],
                     documentType: DocumentType,
                     endTime: LocalDate,
                     authorKey: String
                   )

object Document {
  implicit val encoder: Encoder[Document] = deriveEncoder
  implicit val decoder: Decoder[Document] = deriveDecoder

  val tupled = (this.apply _).tupled
}

sealed trait DocumentType extends EnumEntry

object DocumentType extends Enum[DocumentType] with CirceEnum[DocumentType] {
  val values: immutable.IndexedSeq[DocumentType] = findValues

  case object File extends DocumentType

  case object Text extends DocumentType

}

class Documents(tag: Tag) extends Table[Document](tag, "documents") with SlickEnumSupport {

  implicit lazy val documentTypeMapper = mappedColumnTypeForEnum(DocumentType)

  override val profile = slick.jdbc.PostgresProfile

  def id: Rep[Option[Int]] = column[Option[Int]]("id", O.PrimaryKey, O.AutoInc)

  def documentType: Rep[DocumentType] = column[DocumentType]("documentType")

  def endTime: Rep[LocalDate] = column[LocalDate]("endTime")

  def authorKey: Rep[String] = column[String]("authorKey")

  override def * =
    (
      id,
      documentType,
      endTime,
      authorKey
    ) <> (Document.tupled, Document.unapply)
}

object DocumentsRepository {
  val all = TableQuery[Documents]
}