package ffs.models

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._
import slick.lifted.Tag
import slick.jdbc.PostgresProfile.api._

case class DocumentFile (
                          id: Option[Int],
                          path: String,
                          originalName: String
                        )

object DocumentFile {
  implicit val encoder: Encoder[DocumentFile] = deriveEncoder
  implicit val decoder: Decoder[DocumentFile] = deriveDecoder

  val tupled = (this.apply _).tupled
}

class DocumentFiles(tag: Tag) extends Table[DocumentFile](tag, "documentFiles") {
  def id: Rep[Option[Int]] = column[Option[Int]]("id", O.PrimaryKey, O.AutoInc)

  def path: Rep[String] = column[String]("path")

  def originalName: Rep[String] = column[String]("originalName")

  override def * =
    (
      id,
      path,
      originalName
    ) <> (DocumentFile.tupled, DocumentFile.unapply)
}

object DocumentFilesRepository {
  val all = TableQuery[DocumentFiles]
}