package ffs

import java.io.File
import java.util.UUID

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.{HttpCookie, HttpCookiePair, RawHeader}
import akka.http.scaladsl.server.directives.FileInfo
import akka.http.scaladsl.server.{AuthorizationFailedRejection, _}
import com.typesafe.scalalogging.Logger
import ffs.exceptions.NotFoundException
import ffs.helpers.{RequestValidator, UuidGenerator}
import ffs.models.DocumentVersion
import ffs.services.{DocumentFilesService, DocumentService, DocumentVersionsService}

import scala.concurrent.ExecutionContext

class Router(
              helper: RequestValidator,
              documentService: DocumentService,
              documentVersionsService: DocumentVersionsService,
              documentFilesService: DocumentFilesService,
              uuidGenerator: UuidGenerator,
              logger: Logger
            )(implicit ec: ExecutionContext) extends Directives {

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  implicit def myHandler: ExceptionHandler = ExceptionHandler {
    case e: NotFoundException =>
      logger.error(s"${e.getMessage}")
      complete(StatusCodes.NotFound, e.getMessage)
    case e =>
      logger.error(s"${e.getMessage}")
      complete(StatusCodes.InternalServerError, e.getMessage)
  }

  implicit def myRejectionHandler: RejectionHandler = RejectionHandler.newBuilder()
    .handle {
      case ValidationRejection(msg, _) =>
        msg match {
          case "400" => complete(StatusCodes.BadRequest)
        }
      case AuthorizationFailedRejection =>
        respondWithHeader(RawHeader("WWW-Authenticate","Custom realm=\"FFS\"")) {
          complete(StatusCodes.Unauthorized)
        }
    }
    .result()

  def routes: Route = Route.seal {
    path("auth") {
      get {
        setCookie(HttpCookie("authorKey", value = uuidGenerator.get.toString)) {
          complete(StatusCodes.OK, "Authorized")
        }
      }
    } ~
      path("all") {
        get {
          customAuthentication { cookie =>
            complete(documentService.index(cookie.value))
          }
        }
      } ~ pathPrefix("documents") {
      getRouterForDocuments
    } ~ pathPrefix("documentVersion") {
      getRouterForDocumentVersions
    } ~ pathPrefix("file") {
      getRouterForFile
    }
  }

  def getRouterForDocuments: Route = Route{
    pathEnd {
      customAuthentication { cookie =>
        (post & entity(as[Map[String, String]])) { requestBody =>
          validate(helper.isValidDocumentCreationRequest(requestBody), "400") {
            complete(documentService.add(requestBody + (cookie.name -> cookie.value)))
          }
        }
      }
    } ~
      path(IntNumber) { id =>
        delete {
          customAuthentication { cookie =>
            onSuccess(helper.isAuthor(id, cookie.value)) { value =>
              if (value) {
                complete(documentService.delete(id))
              } else {
                complete(StatusCodes.Forbidden)
              }
            }
          }
        }
      }
  }

  def getRouterForDocumentVersions: Route = Route {
    pathEnd {
      getRouterWithoutId
    } ~
    pathPrefix(IntNumber) { id =>
      getRouterWithId(id)
    }
  }

  def getRouterWithoutId: Route = Route {
    get {
      parameters("documentId".as[Int]) { documentId =>
        customCookie { cookie =>
          onSuccess(helper.isVersionsListAllowed(documentId, cookie)) { value =>
            if (value) {
              complete(documentVersionsService.getByDocId(documentId))
            } else {
              respondWithHeader(RawHeader("WWW-Authenticate","Custom realm=\"FFS\"")) {
                complete(StatusCodes.Unauthorized)
              }
            }
          }
        }
      }
    } ~
      (post & entity(as[DocumentVersion])) { documentVersion =>
        customAuthentication { cookie =>
          onSuccess(helper.isValidDocumentVersionCreationRequest(documentVersion)) { isValid =>
            if (isValid) {
              complete(documentVersionsService.add(documentVersion))
            } else {
              complete(StatusCodes.BadRequest)
            }
          }
        }
      }
  }

  def getRouterWithId(id: Int): Route = Route {
    pathEnd {
      getRouterDeleteAndGet(id)
    } ~ {
      getRouterForUpdate(id)
    }
  }

  def getRouterDeleteAndGet(id: Int): Route = Route {
    delete {
      customAuthentication { cookie =>
        onSuccess(helper.isVersionAuthor(id, cookie.value)) { value =>
          if (value) {
            complete(documentVersionsService.delete(id))
          } else {
            complete(StatusCodes.Forbidden)
          }
        }
      }
    } ~
      get {
        customCookie { cookie =>
          onSuccess(helper.isVersionAllowed(id, cookie)) { value =>
            if (value) {
              complete(documentVersionsService.get(id))
            } else {
              respondWithHeader(RawHeader("WWW-Authenticate","Custom realm=\"FFS\"")) {
                complete(StatusCodes.Unauthorized)
              }
            }
          }
        }
      }
  }

  def getRouterForUpdate(id: Int): Route = Route {
    path("openAccess") {
      get {
        customAuthentication { cookie =>
          onSuccess(helper.isVersionAuthor(id, cookie.value)) { value =>
            if (value) {
              complete(documentVersionsService.openAccess(id))
            } else {
              complete(StatusCodes.Forbidden)
            }
          }
        }
      }
    } ~
    path("closeAccess") {
      get {
        customAuthentication { cookie =>
          onSuccess(helper.isVersionAuthor(id, cookie.value)) { value =>
            if (value) {
              complete(documentVersionsService.closeAccess(id))
            } else {
              complete(StatusCodes.Forbidden)
            }
          }
        }
      }
    }~
    path("attachFile") {
      post {
        customAuthentication { cookie =>
          onSuccess(helper.isVersionAuthor(id,cookie.value)) { isAuthor =>
            if (isAuthor) {
              canAttach(id) { can =>
                storeAndValidateFile {
                  case (meta,file) =>
                    complete(documentVersionsService.attachFile(id,file,meta))
                }
              }
            } else {
              complete(StatusCodes.Forbidden)
            }
          }
        }
      }
    }
  }

  def getRouterForFile: Route = Route {
    path(IntNumber) { fileId =>
      get {
        customCookie { cookie =>
          onSuccess(helper.isFileAllowed(fileId, cookie)) { value =>
            if (value) {
              onSuccess(documentFilesService.get(fileId)) { file =>
                getFromFile(file.path)
              }
            } else {
              complete(StatusCodes.Forbidden)
            }
          }
        }
      }
    }
  }

  def customAuthentication: Directive1[HttpCookiePair] = {
    customCookie flatMap {
      case Some(key) => provide(key)
      case None => reject(AuthorizationFailedRejection)
    }
  }

  def canAttach(id: Int): Directive1[Unit] = {
    onSuccess(helper.canAttach(id)).tflatMap {
      case value if !value._1 => reject(ValidationRejection("400"))
      case _ => provide()
    }
  }

  def customCookie: Directive1[Option[HttpCookiePair]] = {
    optionalCookie("authorKey")
  }

  def storeFile:Directive1[(FileInfo, File)] = {
    storeUploadedFile("file", fileInfo => File.createTempFile(fileInfo.fileName, ".tmp")).tflatMap {
      case (meta, file) => provide((meta,file))
    }
  }

  def validateFile(tuple: (FileInfo, File)): Directive1[(FileInfo, File)] = {
    validate(helper.isValidFile(tuple), "400") & provide(tuple)
  }

  def storeAndValidateFile: Directive1[(FileInfo, File)] = {
    storeFile.flatMap(validateFile)
  }
}
