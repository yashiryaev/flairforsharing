package ffs.database

import ffs.models._
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Future

class Schema {
  val db = Database.forConfig("mydb")

  def run: Future[Unit] = {
    db.run(DocumentsRepository.all.schema.create)
    db.run(DocumentVersionsRepository.all.schema.create)
    db.run(DocumentFilesRepository.all.schema.create)
  }
}
