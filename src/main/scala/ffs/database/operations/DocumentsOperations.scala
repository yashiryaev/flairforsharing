package ffs.database.operations

import java.time.LocalDate

import ffs.database.Schema
import ffs.exceptions.NotFoundException
import ffs.models.{Document, DocumentsRepository}
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}

class DocumentsOperations(schema: Schema)(implicit ec: ExecutionContext) {
  val documents = DocumentsRepository.all

  def add(document: Document): Future[Document] = {
    schema.db.run((documents returning documents.map(_.id)
      into ((doc, id) => doc.copy(id = id))
      ) += document)
  }

  def get(id: Int): Future[Document] = {
    schema.db.run(documents.filter(_.id === id).filter(_.endTime > LocalDate.now()).result.headOption).map(_.getOrElse(throw new NotFoundException))
  }

  def index(authorKey: String): Future[Seq[Document]] = {
    schema.db.run(documents.filter(_.authorKey === authorKey).filter(_.endTime > LocalDate.now()).result)
  }

  def delete(id: Int): Future[Document] = {
    val query = documents.filter(_.id === id).filter(_.endTime > LocalDate.now())
    val action = for {
      results <- query.result
      _ <- query.delete
    } yield results

    schema.db.run(action).map(seq => seq.head)
  }
}
