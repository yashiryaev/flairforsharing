package ffs.database.operations

import ffs.database.Schema
import ffs.exceptions.NotFoundException
import ffs.models.{DocumentFile, DocumentFilesRepository}
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}

class DocumentFilesOperations(schema: Schema)(implicit ec: ExecutionContext) {
  import DocumentFilesRepository._

  val documentFiles = DocumentFilesRepository.all

  def add(file: DocumentFile):Future[DocumentFile] = {
    schema.db.run((documentFiles returning documentFiles.map(_.id)
      into ((f, id) => f.copy(id = id))
      ) += file)
  }

  def get(id: Int): Future[DocumentFile] = {
    val query = documentFiles.filter(_.id === id)
    schema.db.run(query.result.headOption).map(_.getOrElse(throw new NotFoundException))
  }
}
