package ffs.database.operations

import java.time.LocalDate

import ffs.database.Schema
import ffs.exceptions.NotFoundException
import ffs.models.{DocumentFile, DocumentVersion, DocumentVersionsRepository}
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}

class DocumentVersionsOperations(schema: Schema)(implicit ec: ExecutionContext) {
  import DocumentVersionsRepository._
  val documentVersions = DocumentVersionsRepository.all

  def add(documentVersion: DocumentVersion): Future[DocumentVersion] = {
    schema.db.run((documentVersions returning documentVersions.map(_.id)
      into ((version, id) => version.copy(id = id))
      ) += documentVersion)
  }

  def get(id: Int): Future[DocumentVersion] = {
    val versionQuery = documentVersions.filter(_.id === id)
    schema.db.run(versionQuery.withDocument.result.headOption).flatMap { tuple =>
      if (tuple.getOrElse(throw new NotFoundException)._2.endTime.isAfter(LocalDate.now())) {
        schema.db.run(versionQuery.result.headOption).map(_.getOrElse(throw new NotFoundException))
      } else {
        throw new NotFoundException
      }
    }
  }

  def getWithFile(id: Int): Future[(DocumentVersion, DocumentFile)] = {
    val query = documentVersions.filter(_.id === id)
    schema.db.run(query.withFile.result.headOption).map { tuple =>
      tuple.getOrElse(throw new NotFoundException)
    }
  }

  def getAllByDocumentId(docId: Int): Future[Seq[DocumentVersion]] = {
    val versionQuery = documentVersions.filter(_.documentId === docId)
    schema.db.run(versionQuery.withDocument.result.headOption).flatMap { tuple =>
      if (tuple.getOrElse(throw new NotFoundException)._2.endTime.isAfter(LocalDate.now())) {
        schema.db.run(versionQuery.result)
      } else {
        throw new NotFoundException
      }
    }
  }

  def getByFileId(fileId: Int): Future[DocumentVersion] = {
    val versionQuery = documentVersions.filter(_.fileId === fileId)
    schema.db.run(versionQuery.withDocument.result.headOption).flatMap { tuple =>
      if (tuple.getOrElse(throw new NotFoundException)._2.endTime.isAfter(LocalDate.now())) {
        schema.db.run(versionQuery.result.headOption).map(_.getOrElse(throw new NotFoundException))
      } else {
        throw new NotFoundException
      }
    }
  }

  def delete(id: Int): Future[DocumentVersion] = {
    val query = documentVersions.filter(_.id === id)
    val action = for {
      results <- query.result
      _ <- query.delete
    } yield results
    schema.db.run(action).map(seq => seq.head)
  }

  def openAccess(id: Int): Future[Int] = {
    val q = for {dv <- documentVersions if dv.id === id} yield dv.isAllowedAccess
    val updateAction = q.update(Some(true))

    schema.db.run(updateAction)
  }

  def closeAccess(id: Int): Future[Int] = {
    val q = for {dv <- documentVersions if dv.id === id} yield dv.isAllowedAccess
    val updateAction = q.update(Some(false))

    schema.db.run(updateAction)
  }

  def attachFile(id: Int, fileId: Int): Future[(DocumentVersion, DocumentFile)] = {
    val q = for {dv <- documentVersions if dv.id === id} yield dv.fileId
    val updateAction = q.update(Some(fileId))

    schema.db.run(updateAction) flatMap { n =>
      getWithFile(id)
    }
  }
}
