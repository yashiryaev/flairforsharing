package ffs.helpers

import java.util.UUID

class UuidGenerator {
  def get: UUID = {
    UUID.randomUUID()
  }
}
