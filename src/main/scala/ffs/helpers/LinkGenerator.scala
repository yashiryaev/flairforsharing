package ffs.helpers

import com.typesafe.config.Config

class LinkGenerator(config: Config) {
  private lazy val host = config.getString("host")

  def generateLinkOnDocVersion(id: Int): String = {
    s"http://$host/documentVersion/$id"
  }
}
