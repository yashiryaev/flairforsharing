package ffs.helpers

import java.io.File

import ffs.database.operations.{DocumentVersionsOperations, DocumentsOperations}
import ffs.models.{DocumentType, DocumentVersion}
import akka.http.scaladsl.model.headers.HttpCookiePair
import akka.http.scaladsl.server.directives.FileInfo
import ffs.exceptions.NotFoundException

import scala.concurrent.{ExecutionContext, Future}

class RequestValidator(
                        documentsOperations: DocumentsOperations,
                        documentVersionsOperations: DocumentVersionsOperations
                      )(implicit ec: ExecutionContext) {

  def isValidDocumentCreationRequest(requestFields: Map[String, String]): Boolean = {

    @scala.annotation.tailrec
    def isAllRequiredFields(acc: Boolean, required: List[String], req: Map[String, String]): Boolean = required match {
      case Nil => acc
      case head :: tail => isAllRequiredFields(acc && req.contains(head), tail, req)
    }

    val requiredFields = List("documentType", "endTime", "title")
    if (isAllRequiredFields(acc = true, requiredFields, requestFields)) {
      requestFields.get("documentType") match {
        case Some("Text") => requestFields.contains("text")
        case Some("File") => !requestFields.contains("text")
        case Some(_) => false
      }
    } else {
      false
    }
  }

  def isValidFile(file: (FileInfo,File)): Boolean = {
    if(file._1.fileName.endsWith(".exe") || file._1.fileName.endsWith(".app") || file._2.length > 1024*1024*5) {
      file._2.delete
      false
    } else {
      true
    }
  }

  def isValidDocumentVersionCreationRequest(documentVersion: DocumentVersion): Future[Boolean] = {

    def isDocumentExist: Future[Boolean] = {
      documentsOperations.get(documentVersion.documentId).map(_ => true)
    }

    def isCorrectNumber: Future[Boolean] = {
      documentVersionsOperations.getAllByDocumentId(documentVersion.documentId).map(seq => seq.map(_.versionNumber).max == documentVersion.versionNumber - 1)
    }

    def isSameType: Future[Boolean] = {
      documentsOperations.get(documentVersion.documentId).map(doc =>
        doc.documentType match {
          case DocumentType.File => documentVersion.text.isEmpty && documentVersion.fileId.isEmpty
          case DocumentType.Text => documentVersion.text.isDefined && documentVersion.fileId.isEmpty
        }
      )
    }

    for {
      x <- isDocumentExist
      y <- isCorrectNumber
      z <- isSameType
    } yield x && y && z
  }

  def isVersionsListAllowed(documentId: Int, authorKey: Option[HttpCookiePair]): Future[Boolean] = {
    authorKey match {
      case Some(cookie) => for {
        isAuthor <- isAuthor(documentId, cookie.value)
        isAccessible <- isAccessibleAll(documentId)
      } yield isAuthor || isAccessible
      case None => for {
        isAccessible <- isAccessibleAll(documentId)
      } yield isAccessible
    }
  }

  def isVersionAllowed(versionId: Int, authorKey: Option[HttpCookiePair]): Future[Boolean] = {
    authorKey match {
      case Some(cookie) => for {
        isAuthor <- isVersionAuthor(versionId, cookie.value)
        isAccessible <- isAccessible(versionId)
      } yield isAuthor || isAccessible
      case None => for {
        isAccessible <- isAccessible(versionId)
      } yield isAccessible
    }
  }

  def isFileAllowed(fileId: Int, authorKey: Option[HttpCookiePair]): Future[Boolean] = {
    documentVersionsOperations.getByFileId(fileId).flatMap { ver =>
      isVersionAllowed(ver.id.get, authorKey)
    }
  }

  def canAttach(id: Int): Future[Boolean] = {
    documentVersionsOperations.get(id).flatMap { doc =>
      documentVersionsOperations.getAllByDocumentId(doc.documentId)
        .map(seq => seq.map(_.versionNumber).max).map(_ == doc.versionNumber && doc.fileId.isEmpty)
    }
  }

  def isAuthor(documentId: Int, key: String): Future[Boolean] = {
    documentsOperations.get(documentId).map(doc => doc.authorKey == key)
  }

  def isVersionAuthor(i: Int, key: String): Future[Boolean] = {
    documentVersionsOperations.get(i).map(ver => documentsOperations.get(ver.documentId)).flatMap(fut => fut.map(doc => doc.authorKey == key))
  }

  private def isAccessibleAll(documentId: Int): Future[Boolean] = {
    documentVersionsOperations.getAllByDocumentId(documentId).map(seq => seq.forall(docVersion => docVersion.isAllowedAccess.getOrElse(false)))
  }

  private def isAccessible(versionId: Int): Future[Boolean] = {
    documentVersionsOperations.get(versionId).map(version => version.isAllowedAccess.getOrElse(false))
  }

}
