package ffs

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.Logger
import ffs.database.Schema
import ffs.database.operations.{DocumentFilesOperations, DocumentVersionsOperations, DocumentsOperations}
import ffs.helpers.{LinkGenerator, RequestValidator, UuidGenerator}
import ffs.services.{DocumentFilesService, DocumentService, DocumentVersionsService}

import scala.concurrent.{ExecutionContext, Future}

object Main extends App {

  import ExecutionContext.Implicits.global

  implicit val actorSystem: ActorSystem = ActorSystem()

  val schema = new Schema

  schema.run

  val logger = Logger("FFS")

  val documentOperations = new DocumentsOperations(schema)
  val documentVersionsOperations = new DocumentVersionsOperations(schema)
  val documentFilesOperations = new DocumentFilesOperations(schema)

  val uuidGenerator = new UuidGenerator
  val linkGenerator = new LinkGenerator(ConfigFactory.load("application"))
  val validator = new RequestValidator(documentOperations, documentVersionsOperations)

  val documentFilesService = new DocumentFilesService(documentFilesOperations)
  val documentVersionsService = new DocumentVersionsService(documentVersionsOperations,documentFilesService,linkGenerator)
  val documentService = new DocumentService(documentVersionsService, documentOperations, documentVersionsOperations)

  val router = new Router(validator, documentService, documentVersionsService, documentFilesService, uuidGenerator, logger)

  val handler: Future[Http.ServerBinding] = Http().bindAndHandle(router.routes, "0.0.0.0", 8080)

  logger.info("Application started")
}
