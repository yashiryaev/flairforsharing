package ffs.exceptions

class NotFoundException extends Exception("Entity Not Found")
