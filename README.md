# FlairForSharing

## Introduction
FlairForSharing создан для удобного обмена текстами и небольшими файлами без регистрации.

FlairForSharing was created for convenient exchange of texts and lightweight files without registration.

## Features
FFS позволяет вам создавать тексты и загружать небольшие файлы, вести их историю версий и открывать доступ к ним.

FFS allows you to create texts and upload small files, keep their version history and open access to them.

### API

`GET /auth` - возвращает ваш ключ доступа в виде cookie.

`GET /all` - возвращает все документы и их версии автора.

`GET /auth` - returns your cookie passkey.

`GET /all` - returns all author documents and their versions
#### Documents

`POST /documents` - создает новый документ и его первую версию.<br/>
Обязательные поля для текстов: `documentType:Text`, `endTime:yyyy-MM-dd`, `title:`, `text:`.<br/>
Обязательные поля для файлов: `documentType:File`, `endTime:yyyy-MM-dd`, `title:`

`DELETE /documents/{id}` - удаляет документ и все его версии

`POST /documents` - creates a new document and its first version.<br/>
Required fields for text: `documentType:Text`,`endTime:yyyy-MM-dd`,`title:`,`text:`.<br/>
Required fields for file: `documentType:File`, `endTime:yyyy-MM-dd`, `title:`.<br/>

`DELETE /documents/{id}` - delete document and its versions

#### Document Versions

`GET /documentVersions?documentId` - возвращает список версий документа, принадлежащих документу с переданным documentId.

`POST /documentVersions` - добавляет новую версию документа. <br/>
Обязательные поля для документа типа Text: `documentId`, `versionNumber:{n}`, `title`, `text`.<br/>
Обязательные поля для документа типа File: `documentId`, `versionNumber:{n}`, `title`.<br/>
`n` - целое значение строго на 1 больше значения версии предыдущей версии
 
`POST /documentVersions/{createdDocVersionId}/attachFile` - прикрепление файла к созданной версии документа. Этот запрос необходимо отправлять сразу после создания новой версии документа типа File. 

`GET /documentVersions/{id}` - возвращает версию документа по переданному id.

`DELETE /documentVersions/{id}` - удаляет версию документа по переданному id.

`GET /documentVersions/{id}/openAccess` - открывает доступ к версии документа по переданному id, возвращает строку с URI, по которому можно получить данную версию. Теперь к этой версии могут иметь доступ пользователи без ключа доступа или с другим ключом доступа.

`GET /documentVersions/{id}/closeAccess` - ограничивает доступ к версии документа только для его автора.

`GET /documentVersions?documentId` - returns a list of document versions belonging to the document by the passed documentId.
 
`POST /documentVersions` - creates a new document version.<br/>
Required fields for Text type document version: `documentId`, `versionNumber:{n}`, `title`, `text`.<br/>
Required fields for File type document version: `documentId`, `versionNumber:{n}`, `title`.<br/>
`n` - an integer value strictly 1 greater than the same value of the previous version.

`POST /documentVersions/{createdDocVersionId}/attachFile` - attach a file to created document version. This request must be sent right after creating a File type document version. 

`GET /documentVersions/{id}` - returns document version by the passed id.

`DELETE /documentVersions/{id}` - deletes document version by the passed id.

`GET /documentVersions/{id}/openAccess` - opens access to the document version by the passed id, returns a string with the URI by which this version can be obtained. Now users without a passkey or with a different passkey can access this version.

`GET /documentVersions/{id}/closeAccess` - closes access to the document version by the passed id.

### Files

`GET /file/{id}` - возвращает файл по переданному id.

`GET /file/{id}` - returns file by the passed id.