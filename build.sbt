name := "FlairForShare"

version := "0.1"

scalaVersion := "2.12.11"

libraryDependencies ++= Seq(
  // DB
  "com.typesafe.slick" %% "slick" % "3.3.0",
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.3.0",
  "org.postgresql" % "postgresql" % "9.4-1206-jdbc42",
  // Circe
  "io.circe" %% "circe-core" % "0.12.3",
  "io.circe" %% "circe-generic" % "0.12.3",
  "io.circe" %% "circe-parser" % "0.12.3",
  // akka
  "com.typesafe.akka" %% "akka-http" % "10.1.11",
  "com.typesafe.akka" %% "akka-stream" % "2.6.4",
  "com.typesafe.akka" %% "akka-actor-typed" % "2.6.4",
  "com.typesafe.akka" %% "akka-actor" % "2.6.4",
  "de.heikoseeberger" %% "akka-http-circe" % "1.31.0",
  "com.tethys-json" %% "tethys" % "0.11.0",

  "com.beachape" %% "enumeratum" % "1.6.0",
  "com.beachape" %% "enumeratum-circe" % "1.6.0",
  "com.beachape" %% "enumeratum-slick" % "1.6.0",

  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",

  "com.typesafe.akka" %% "akka-testkit" % "2.6.4" % Test,
  "com.typesafe.akka" %% "akka-http-testkit" % "10.1.11" % Test,
  "org.scalatest" %% "scalatest" % "3.0.8" % Test,
  "org.scalamock" %% "scalamock" % "4.4.0" % Test
)

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

dockerBaseImage := "openjdk:jre-alpine"
enablePlugins(AshScriptPlugin)

mainClass in Compile := Some("Main")